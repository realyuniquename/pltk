#Std lib improvements
(optimizations, generalization, specification)
* Create a specification for recommended code style to be followed in std lib and in any Haxe project.

#IDEA Haxe plugin
* Support compiler-provided code completion. Protocol description: https://haxe.org/manual/cr-completion-server.html
* Interactive debugging for macros. Macro debugger is currently implemented in dev version of Haxe. Protocol description: https://github.com/Simn/haxe/wiki/Eval-Debugger-Protocol
* Improve #if flags support.
	* Currently IDEA incorrectly determines if code in #if block is included in current build setup or not.
	* Support inline #if directives. All types/symbols ets should be correctly recognized. E.g.: var a = #if MyClass.VALUE1 1 #else MyClass.VALUE2 #end;
	* Support arrow functions (added in dev version of Haxe)	
	* Improve support for Haxe type system:
		* Show constructors of available enums in top-level completion. And correctly recognize enum constructors without prepending them with enum name.
			* Normal enums
			* Abstract enums
			E.g.
			import Type;
			ValueType.TFloat;
			TFloat; //this symbol should be handled exactly as TFloat on line above
		* completion for constraints in generic type arguments:
			function<T:MyClass>(a:T) return a.| //list fields of MyClass
			arrayOfStrings.map(function(s) s.|) //list methods for String

#Async/Await
* Basic implementation of single-threaded async/await (see C# async/await behavior as a reference)
* Support async/await for closures
* Correctly handle exceptions in async call stack.
* Provide an ability to obtain current stack of async calls with normal call stack in between

#Generators (yield)
* Basic implementation of generators
* Implement a way to pass values to generator after generator started

#Explicit generic method call
E.g. "Bar.foo<MyClass>()" Instead of being forced to pass generic type as an argument. "function foo<T>(cls:Class<T>)"

#Improve haxelib package management
* Improve integration with git:
	* Treat separate commits, tags and branches as normal lib versions. Allow to have several git versions of library installed simultaneously.
	* Allow to specify commit hash, tag, branch in `-lib` compiler flag. E.g.: -lib hxasync=a42fd12
* Implement per-project libraries installation. E.g. some libs installed in special dir of current project thus only available inside this project.
* Provide an ability to list `-D` flags for a library in haxelib.json. So that lib.haxe.org site can list library-specific flags on s library page.
* Create a specification which will standardize libraries structure, -D flags naming and `haxelib run` tools.

#Support ES-6 base syntax

#Safe-navigation operator
* Implement ?? operator in Haxe: "a ?? b" should return "a" if it's not null and "b" otherwise/
* Implement elvis operator in Haxe: var companyName = user?.phone?.company?.name; - "companyName" should become null if any symbol postfixed with ?. is null

#Implement "=" operator overloading for abstract
Support assignment overloading in abstract types.
E.g.:
abstract Kilometers(Float) {
	@:op(A = B) inline function(m:Meters) this = m / 1000
}

var m:Meters = 100;
var k:Kilometers = m;
trace(k); //prints 0.1

#Compiler meta generalization
* Create detailed documentation with examples for each built-in compiler meta (current doc: https://haxe.org/manual/cr-metadata.html)

#Rest arguments
* Implement support for rest arguments in Haxe.
	E.g.:
		function format(tempate:String, ...values:Array<Dynamic>):String;

		format('Hello, {0} and ', 'world', 'underworld');